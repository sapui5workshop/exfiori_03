sap.ui.define([
		"com/demo/s276/ex03/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("com.demo.s276.ex03.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);