jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
		"sap/ui/test/Opa5",
		"com/demo/s276/ex03/test/integration/pages/Common",
		"sap/ui/test/opaQunit",
		"com/demo/s276/ex03/test/integration/pages/Worklist",
		"com/demo/s276/ex03/test/integration/pages/Object",
		"com/demo/s276/ex03/test/integration/pages/NotFound",
		"com/demo/s276/ex03/test/integration/pages/Browser",
		"com/demo/s276/ex03/test/integration/pages/App"
	], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.demo.s276.ex03.view."
	});

	sap.ui.require([
		"com/demo/s276/ex03/test/integration/WorklistJourney",
		"com/demo/s276/ex03/test/integration/ObjectJourney",
		"com/demo/s276/ex03/test/integration/NavigationJourney",
		"com/demo/s276/ex03/test/integration/NotFoundJourney",
		"com/demo/s276/ex03/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});