sap.ui.define([
	"com/demo/s276/ex03/controller/BaseController",
	"sap/ui/core/routing/History",
	"sap/m/MessageToast"
], function(BaseController, History, MessageToast) {
	"use strict";

	return BaseController.extend("com.demo.s276.ex03.controller.Edit", {

		onInit: function(){
			this.getRouter().getRoute("edit").attachPatternMatched(this._onRouteMatched, this);
		},
		
		_onRouteMatched: function(oEvent){
			var oModel = this.getModel();
			oModel.updateBindings(true);
			oModel.setDefaultBindingMode("OneWay");
			
			var sObjectId = oEvent.getParameter("arguments").objectId;
			oModel.metadataLoaded().then(function(){
				var sObjectPath = this.getModel().createKey("ProductSet", {
					ProductID : sObjectId
				});
				this.getView().bindElement({path: "/" + sObjectPath});
			}.bind(this));
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.demo.s276.ex03.view.Edit
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.demo.s276.ex03.view.Edit
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.demo.s276.ex03.view.Edit
		 */
		//	onExit: function() {
		//
		//	}
		
		onNavBack: function(){
			this.getModel().setDefaultBindingMode("TwoWay");
			
			var oHistory = History.getInstance(),
				sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				// The history contains a previous entry
				history.go(-1);
			} else {
				// Otherwise we go backwards with a forward history
				var bReplace = true;
				this.getRouter().navTo("worklist", {}, bReplace);
			}
		},
		
		onCancel: function(){
			this.onNavBack();
		},
		
		onSave: function(){
			var oContext = this.getView().getBindingContext();
			var sPath = oContext.getPath();
			var sProductName = this.byId("nameField").getValue();
			var sProductID = oContext.getProperty("ProductID");

			var oObject = {};
			oObject.Name = sProductName;
			oObject.Category = this.byId("categoryField").getValue();
			oObject.SupplierID = this.byId("supplierIDField").getValue();
			oObject.Price = this.byId("priceField").getValue();
			oObject.CurrencyCode = this.byId("priceField").getUnitOfMeasure();

			this.getModel().update(sPath, oObject, {
				    success: function(){
						this.getModel().setDefaultBindingMode("TwoWay");
						
						// navigate to the new product's object view
						this.getRouter().navTo("object", {
							objectId : sProductID
						}, true);
				
						// unbind the view to not show this object again
						this.getView().unbindObject();
						
						MessageToast.show(this.getResourceBundle().getText("worklistEditProductSuccess", [sProductName]), {
							closeOnBrowserNavigation : false	
						});
					}.bind(this),
					error: function(){
						MessageToast.show(this.getResourceBundle().getText("worklistEditProductError", [sProductName]));
					}.bind(this)
			});				
		}

	});

});